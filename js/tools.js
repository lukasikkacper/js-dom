var rates = [];

function getData()
{
  fetch('https://api.nbp.pl/api/exchangerates/tables/C/?format=json')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      rates = data[0].rates;
      addOptions();
  });
}

function addOptions()
{
  for(var i = 0; i < rates.length; i++)
  {
    let option = document.createElement("option");
    option.appendChild(document.createTextNode(rates[i].code + " - " + rates[i].currency));
    option.value = rates[i].ask;

    document.getElementById("currency").appendChild(option);
  }

  createSelect();
}

function convert()
{
  let value = parseFloat(document.getElementById("currency").value);
  let plnCount = parseFloat(document.getElementById("pln").value);

  let x = document.getElementById("currency");

  if(!isNaN(plnCount))
  {
    let p = document.getElementById("result");
    let cur = x[x.selectedIndex].text.substring(0,3);
    
    let res = Math.decimal(plnCount/value,2);

    p.innerHTML = `${plnCount} PLN to: ${res} ${cur}`;
  }
  else
  {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Pole złotówek nie może być puste!',
    });
  }
}

Math.decimal = function(n, k) 
{
    var factor = Math.pow(10, k+1);
    n = Math.round(Math.round(n*factor)/10);

    return n/(factor/10);
}

document.addEventListener("DOMContentLoaded",getData());

document.getElementById("convertBtn").addEventListener("click", convert);
