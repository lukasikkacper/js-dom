var NumberInput = document.getElementById("numberInput");
var NameInput = document.getElementById("nameInput");

var NumberError = document.getElementById("numberError");
var NameError = document.getElementById("nameError");

var AddBtn = document.getElementById("addBtn");

var removeButtons = document.querySelectorAll(".removeEpisode");
var editButtons = document.querySelectorAll(".editEpisode");

document.addEventListener("DOMContentLoaded", init);

function init()
{
    NameInput.addEventListener("input", _.debounce(onChangleValidate,500,{}));
    NumberInput.addEventListener("change", onChangleValidate);
    AddBtn.addEventListener("click", addEpisode);

    removeButtons.forEach(function(element){
        element.addEventListener("click",removeEpisode);
    });

    editButtons.forEach(function(element){
        element.addEventListener("click",function(event){
            getEditor(event.target);
        });
    });
}

function validate()
{
    var isPassed = true;

    if(NumberInput.value == "")
    {
        addError(NumberInput,NumberError,"Pole nie może być puste !");

        isPassed = false;
    }
    else if(isNaN(parseInt(NumberInput.value)))
    {
        addError(NumberInput,NumberError,"Numer nie jest liczba !"); 

        isPassed = false;
    }
    else if(getEpisodeNumbers().includes(NumberInput.value))
    {
        addError(NumberInput,NumberError,"Istnieje taki numer odcinka !"); 

        isPassed = false;
    }
    else
    {
        removeError(NumberInput,NumberError);
    }

    if(NameInput.value == "")
    {
        addError(NameInput,NameError,"Pole nie może być puste !");

        isPassed = false;
    }
    else
    {
        removeError(NameInput, NameError);
    }

    return isPassed;
}

function onChangleValidate()
{
    if(!validate())
    {
        AddBtn.disabled = true;
        return;
    }
    
    AddBtn.disabled = false;
}

function addError(input,error,msg)
{
    input.classList.add("error");
    error.innerHTML = msg;
}

function removeError(input,error)
{
    input.classList.remove("error");
    error.innerHTML = "";
}

function sortTable(selector) 
{
    var table, rows, switching, i, x, y, shouldSwitch;

    table = document.querySelector(selector);
    switching = true;

    while (switching) {
      switching = false;
      rows = table.rows;

      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false;

        x = rows[i].getElementsByTagName("TD")[0];
        y = rows[i + 1].getElementsByTagName("TD")[0];

        if (parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
          shouldSwitch = true;
          break;
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
      }
    }
}

function getEpisodeNumbers()
{
    var EpisodeNumbers = [];

    var table = document.querySelector("table");
    var rows = table.rows;

    for(var i = 1; i < rows.length; i++)
    {
        var x = rows[i].getElementsByTagName("TD")[0];
        EpisodeNumbers.push(x.innerHTML);
    }

    return EpisodeNumbers;
}

function removeEpisode(event)
{
    var tr = event.target.parentNode.parentNode.parentNode;

    Swal.fire({
        title: 'Chcesz usunąć ?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Nie',
        confirmButtonText: 'Tak',
        confirmButtonColor: '#BADA55'
      }).then((result) => {
        if (result.value) {
          tr.remove();
          Swal.fire(
            'Usunięto!',
            'Epizod został usunięty !',
            'success'
          )
        }
      })
}

function addEpisode()
{
    var table = document.querySelector("table");
    var tr = document.createElement("tr");
    var tdNumber = document.createElement("td");
    var tdName = document.createElement("td");
    var tdRemove = document.createElement("td");
    var div = document.createElement("div");
    var i = document.createElement("i");
    var editi = document.createElement("i");

    i.classList = "removeEpisode far fa-trash-alt";
    i.addEventListener("click",removeEpisode);
    editi.classList = "editEpisode far fa-edit";
    editi.addEventListener("click",getEditor);
    tdRemove.appendChild(i);
    tdRemove.appendChild(editi);
    //data-toggle="tooltip" title="Istnieje już taki numer odcinka !"

    tdNumber.appendChild(document.createTextNode(NumberInput.value));
    tdName.appendChild(document.createTextNode(NameInput.value));

    tr.appendChild(tdNumber);
    tr.appendChild(tdName);
    tr.appendChild(tdRemove);
    tr.setAttribute("data-toggle", "tooltip");
    tr.setAttribute("title", "Istnieje już taki numer odcinka !");
    table.appendChild(tr);

    sortTable("table");

    NameInput.value = "";
    NumberInput.value = "";

    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Dodano odcinek',
        showConfirmButton: false,
        timer: 1500
      })
}

async function getEditor(target){
    let data = target.parentNode.parentNode.parentNode.childNodes;

    let number = data[1].innerHTML;
    let name = data[3].innerHTML;

    const { value: formValues } = await Swal.fire({
      title: 'Edycja odcinka',
      html:
        '<input type="number" id="number-edit" class="swal2-input" value="'+number+'" required>' +
        '<input id="name-edit" class="swal2-input" value="'+name+'" required>',
      focusConfirm: false,
      preConfirm: () => {
        return [
          document.getElementById('number-edit').value,
          document.getElementById('name-edit').value
        ]
      }
    });

    if(formValues)
    {
        data[1].innerHTML = formValues[0];
        data[3].innerHTML = formValues[1];
    }
    
    validateNumber();
}

function validateNumber()
{ 
  var table = document.querySelector("table");
  var rows = table.rows;

  for(var i = 1; i < rows.length; i++)
  {
    var a = rows[i].getElementsByTagName("td")[0];
    rows[i].style.background = "transparent";
    $(rows[i]).tooltip("dispose");

    for(var j = 1; j < rows.length; j++)
    {
      if(i==j)
        break;

      var b = rows[j].getElementsByTagName("td")[0];

        if(a.innerHTML == b.innerHTML)
        {
          rows[i].style.background = "#FFB7B7";
          rows[j].style.background = "#FFB7B7";

          $(rows[i]).tooltip();
          $(rows[j]).tooltip();
        }
    }
  }

}